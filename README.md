# Description:

 Project name: School Management written in Java

 This project can manage students and teachers in a school. It can manage the money the school it wins and the ones it spends.
 Earnings comes from students who pay their fees and the expenses are the salaries of the teachers. The student has a name, an id 
and taxes. The teacher also has a name, an id and a salary.

# Setup:

 In this project I used Map<Integer, Object> interface from collections (the most important).

# Further Work:

 For the next two days, I want to work to a log console interface, where new teachers and students can be added, with basic 
personal details.

 Future -> : In the coming days I want to use an abstract class and a interface, both created by me and a few exceptions, a class
also created by me.

 In near future -> : I want to create a database in MongoDB and send invoices to teacher in pdf format (maybe on students to).

