package schoolmanagement;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Teacher claudiu = new Teacher(1, "Claudiu", 2800);
        Teacher mihaela = new Teacher(2, "Mihaela", 3400);
        Teacher roxana = new Teacher(3, "Roxana", 2900);

        List<Teacher> teacherList = new ArrayList<Teacher>();
        teacherList.add(claudiu);
        teacherList.add(mihaela);
        teacherList.add(roxana);

        Student stefan = new Student(1, "Stefan", 7);
        Student robin = new Student(2, "Robin", 7);
        Student luisa = new Student(3, "Luisa", 5);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(stefan);
        studentList.add(robin);
        studentList.add(luisa);

        School newSchool = new School(teacherList, studentList);

        robin.payFees(500);

        System.out.println("Total money earned " + newSchool.getTotalMoneyEarned() + " $");

    }
}
